# Python Tello Course
Programming Tello Edu Drones with Python

## Learning goals
1. Basics of networking
2. General drone handling techniques
3. Tello Edu Remote Control mechanism
4. Rudimentary Python Programming
5. Drone swarming - planning and programming

## For students
Jump right into the [lesson notes](./lesson-notes/README.md)!

## For teachers
Before programming, it's often the hurdle of setting up the environment. Often we tumbles multiple times to get there the first time. Every time if we didn't prepare a reproducible environment.

It's what this course provides - a reproduce environment - Ubuntu running on a VM. Perfectly reproducible.

See [environment setup](environment-setup/README.md) for details.

Interested students are encouraged to explore as well :) Be your own teacher!
