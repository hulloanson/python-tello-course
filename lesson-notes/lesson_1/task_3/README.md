# Lesson 1 - Task 3
## Goal
Fly Tello with **MORE COMMANDS**!

## Steps
1. Open Packet Sender.
2. Find out the IP of your drone (how? [see cheatsheet](../../../cheatsheets/tello.md#what-is-my-tellos-ip-and-port))
2. Switch Tello to command mode (remember [task 1](../task_1/README.md#switching-tello-to-command-mode)! You've done it twice!)

3. Here are more commands you can try: (distances are numbers measured in cm)

    > Take care to not knock into things. Think before you send the command! Be ready to [interfere manually](../../../cheatsheets/cautions.md#what-to-do-if-your-tello-go-crazy) if it goes crazy.

    - `forward [distance]`
    - `back [distance]`
    - `right [distance]`
    - `left [distance]`

    e.g. `forward 10` means "Tello, move 10cm forward"

4. Land it when you're done. (Of course!)

## Tip: I type really slow and Tello switches off before I hit send / I hate typing
[Save the packets](../../../cheatsheets/tello.md) and send them by clicks.

## Assessment
Show it to your lecturer / assistant : )

## Any problems? Solutions?
Jot it down in your notebook / cheatsheets!

## Continue to Task 4!
[Swoosh](../task_4/README.md)