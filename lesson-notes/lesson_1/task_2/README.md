# Lesson 1 - Task 2
## Goal
Fly Tello with commands!

## Steps
1. Open Packet Sender.
2. Find out the IP of your drone (how? [see Tello Cheatsheet](../../../cheatsheets/tello.md#what-is-my-tellos-ip-and-port))
2. Switch Tello to command mode (how? Remember [task 1](../task_1/README.md#switching-tello-to-command-mode)?)

(now we take off! Make sure the Tello has enough space)

3. Send `takeoff` to Tello. See if it takes off!

(and now we land it)

4. Send `land`. It should now land

## Assessment
Show it to your lecturer / assistant : )

## Any problems? Solutions?
Jot it down in your notebook / cheatsheets!

## Continue to Task 3!
[Portal](../task_3/README.md)
