# Lesson 1
## Welcome!
Since you have learnt how Tello talks, it's time to actually talk with it!

(In lame speech is you learn Tello's SDK and send commands to it over UDP.)

(Scroll over if you don't understand.)

## Before the lesson
Have a look at the [Caution sheet](../../cheatsheets/cautions.md) - it is important.

## Get ready
VMWare Player > Double click Ubuntu 
<small>(For cheatsheet on to use Ubuntu, see [VM Usage Cheatsheet](../../cheatsheets/vm-usage.md))</small>

## What you will need
1. Packet Sender
2. Your browser - to read lesson notes : )
3. Your Tello Edu drone (1 only)
4. 4 batteries

## Tasks
1. [Task 1](task_1/README.md)
1. [Task 2](task_2/README.md)
1. [Task 3](task_3/README.md)
1. [Task 4](task_4/README.md)
1. [Task 5](task_5/README.md)

## Takeaways
See the this lesson's [summary](summary.md).
