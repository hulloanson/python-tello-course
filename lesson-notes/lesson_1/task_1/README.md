# Lesson 1 - Task 1
## Goal
Set Tello to command mode

## Before we start...
Let's reset the Tello so it is in AP mode
### How?
See [Tello Cheatsheet](../../../cheatsheets/tello.md#reset-your-tello-it-returns-to-ap-mode)

## Steps
Please read through this whole note before you start : )

Basically you just need to send 1 packet to Tello so it enters command mode

### Switching Tello to command mode
1. Turn on your Tello. It is now in AP mode (what? don't know what is AP mode? [Read the Tello cheatsheet](../../../cheatsheets/tello.md#differences-between-ap-mode-and-station-mode)).
2. Connect to your Tello's AP ([Tello Cheatsheet - What is the AP name?](#what-is-the-ap-name))
3. Start Packet Sender
4. Send `command` to your Tello ([Refer to Tello cheatsheet - Send a command](../../../cheatsheets/tello.md#send-a-command-to-tello-in-packet-sender))

#### What should happen?
Your Tello should now blink a steady green light

### I type really slow and Tello switches off before I hit send / I hate typing
[Save the packets](../../../cheatsheets/tello.md) and send them by clicks.

## Assessment
Show it to your lecturer / assistant : )

## Any problems? Solutions?
Jot it down in your notebook / cheatsheets!

## Continue to Task 2!
[Here](../task_2/README.md)