# Lesson 1 - Task 4
## Goal
Let's get adventurous.

## Steps
1. Open Packet Sender.

2. Switch Tello to command mode ([don't you dare to forget it](../task_1/README.md#switching-tello-to-command-mode). You've done it thrice.)

3. Turn it, flip it:
    - Rotate clockwise: `cw [deg, 1 to 360. no negative numbers please]`
    - Rotate counter-clockwise: `ccw [deg]`
    - `flip [direction]` 
      - `direction` can be `l` (left) or `r` (right) or `f` (forward) or `b` (back)
    > **Caution** - leave at least 50cm around the drone to not hit someone / something

    e.g. `flip r`
    e.g. `ccw 90`

4. Land it when you're done.

## Assessment
Show it to your lecturer / assistant : )

## Any problems? Solutions?
Jot it down in your notebook / cheatsheets!

## Continue to Task 5!
[Go!](../task_5/README.md) It's the last one!