# Lesson 1 Summary

## Introduction to network
See the [Network Concept Cheatsheet](./../../cheatsheets/network.md)

## The differences between AP mode and Station mode
Read [Tello Cheatsheet - Differences between AP mode and station mode](../../cheatsheets/tello.md#differences-between-ap-mode-and-station-mode)

## BEFORE YOU DO ANYTHING
Change Tello to command mode so it responds to you
### How?
Send `command` to the Tello

## Resetting Tello to AP mode
As detailed in [Tello Cheatsheet - Reset your Tello](../../cheatsheets/tello.md#reset-your-tello-it-returns-to-ap-mode)

## Determine Tello's IP on a WI-FI network 
As detailed in [Tello Cheatsheet - Scan for your Tellos](../../cheatsheets/tello.md#scan-for-your-tellos)

## Send packet to Tello in Packet Sender
As detailed in [Tello Cheatsheet with the same title](../../cheatsheets/tello.md#send-a-command-to-tello-in-packet-sender)

## Basic flying commands
e.g. forward, backward, flip, rotate, etc. with commands

For more commands see the [Command Cheatsheet](../../cheatsheets/commands.md)!

<!-- TODO: command cheatsheet -->

## Cautions
Read the [Caution Cheatsheet](../../cheatsheets/cautions.md)

_[Tello Cheatsheet](../../cheatsheets/tello.md) contains many other tricks! Look at it from time to time : )_

## Troubleshooting
Part of programming (drones in particular!) is all about troubleshooting, or in formal term, debugging.

So make use of the [Troubleshooting cheatsheet](../../cheatsheets/troubleshooting.md) in this course's journey! It is in no way complete though. Never stop to write your own : )