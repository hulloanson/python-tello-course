# Lesson Notes
## Contents
Here you can find:

1. Notes for each lessons
2. Quizzes
3. Cheatsheets

    - [Tello Cheatsheet](../cheatsheets/tello.md)
    - [Python Cheatsheet](../cheatsheets/python.md)
    - [Commands Cheatsheet](../cheatsheets/commands.md)
    - [Troubleshooting Cheatsheet](../cheatsheets/troubleshooting.md)
    - [Cautions Cheatsheet](../cheatsheets/cautions.md)
    - [Network Cheatsheet](../cheatsheets/network.md)
    - [VM Usage Cheatsheet](../cheatsheets/vm-usage.md)

4. Code files (Python files! .py files).

## Lesson etiquettes
### Ask questions!
No one knows everything. Ask us / fellow classmates if there are any questions.

### Speak out!
Teachers are not perfect too! We are all here to learn : )

Don't hesitate to point out any problems. Any - in course materials, ways of teaching, etc.

### Jot notes!
They will be immensely useful as you learn. Keep them ready, always, and don't forget to write your own cheatsheets! These cheatsheets can never be complete. Write down what you learn. The cheatsheets are: