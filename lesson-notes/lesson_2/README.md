# Lesson 2
## We actually START PROGRAMMING TELLO
Automate! Python!

## Before the lesson
Have a look at the [caution sheet](../cautions.md) - it is important.

## What you will need
1. Visual Studio code
2. Your browser - to read lesson notes : )
3. Your Tellos (you have 4, but we only use 1 for this lesson)
4. 4 batteries

## Tasks
1. [Task 1](task_1/README.md)
2. [Task 2](task_2/README.md)
3. [Task 3](task_3/README.md)
4. [Task 4](task_4/README.md)

## Takeaways
See this lesson's [summary](summary.md)