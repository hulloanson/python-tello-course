from telloswarm.scan import scan
from telloswarm.connection import start_connections, close_connections
from telloswarm.commands import *

# TODO: write your own drone's serial number
serial_numbers = [
    '0TQDG7BEDBJ740'
]

drones = scan(serial_numbers)

connections = start_connections(drones)

# TODO: get connection of your own drone
connection = connections['0TQDG7BEDBJ740']

# TODO: Fly a complete square. Nose is always parallel to the edges.

close_connections(connections)