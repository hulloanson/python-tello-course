# Lesson 2 - Task 5
## Goal
More moves with Tello!

## Steps (in Python!)
1. Open `move_square.py` in vscode.
2. Write some code so it moves a complete square.
    
    ⚠️ _The drone's nose should be in the same direction as the arrows_

      ![Drone flying a square](../../../images/move-square.svg)
## Assessment
Show it to your lecturer / assistant : )

## Any problems? Solutions?
Jot it down in your notebook / cheatsheets!

## Continue to Task 6!
[Here](../task_6/README.md)
