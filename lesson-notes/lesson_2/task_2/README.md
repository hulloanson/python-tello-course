# Lesson 2 - Task 2
## Goal
Set Tello to Station mode

## Before we start...
Let's reset the Tello so it is in AP mode
### How?
See [Tello cheatsheet - Reset Your Tello](../../../cheatsheets/tello.md#reset-your-tello-it-returns-to-ap-mode)

## Steps
Please read through this whole note before you start : )

Basically you send 2 packets to Tello so it

1. enters command mode; and
    - [Tello Cheatsheet - Turn on command mode](../../cheatsheets/tello.md#turn-on-command-mode)
2. enters station mode so it connects to your router
    - [Tello Cheatsheet - Switch to Station mode](../../../cheatsheets/tello.md#switch-to-station-mode)".

After that:

1. We scan for the Tello's IP in the router
    - [Tello Cheatsheet - Scan for your Tellos](../../../cheatsheets/tello.md#scan-for-your-tellos)".

## Assessment
Show it to your lecturer / assistant : )

## Any problems? Solutions?
Jot it down in your notebook / cheatsheets!

## Continue to Task 3!
[Here](../task_3/README.md)
