# Lesson 2 Summary
## Basic Python
Everything we learnt this lesson is in the [Python Cheatsheet](../../cheatsheets/python.md). Have a good look at it!

A brief checklist on what you should have learnt:

<table>
<tbody>
<tr>
<td>

1. [ ] Variables
2. [ ] Functions
3. [ ] String
4. [ ] Integer
</td>
<td>

6. [ ] Array
7. [ ] Dictionary
8. [ ] Indent
9. [ ] Scope
</td>
</tr>
</tbody>
</table>


## Tello Commands
Tello commands is the core to controlling Tello drones. 

In this course, there are 2 ways of telling a Tello what to do:

1. Sending command through UDP (a.k.a the Packet Sender way)
2. Calling Python functions by importing functions from `telloswarm.commands`.

Both are highly similar - sdk commands are directly mapped to function of the same name - `takeoff`, `forward`, etc... For example:

The UDP way:
```plaintext
forward 100
```

The Python way:
```python
forward(connection, 100)
```

Either way, you're highly advised to read through the [Commands Cheatsheet](../../cheatsheets/commands.md) to get familiar with Tello commands can do.