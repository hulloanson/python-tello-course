# Lesson 2 - Task 6
## Goal
Improvise!

## Steps
1. Look at the [Commands Sheet](../../../cheatsheets/commands.md).

2. Design a fly route you like!

## That's it!
Go to the [summary](../summary.md)