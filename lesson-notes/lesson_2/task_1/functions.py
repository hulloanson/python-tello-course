# This is a comment. It has absolutely no effects
# This is an import. You import functions other writes
from telloswarm.scan import scan
import telloswarm.commands
from telloswarm.connection import start_connections, close_connections


# def means "define". So we define functions down here.
# What is a function? 
# You give it something. It do something (and possibly give you back something).

# These are the functions that don't give you back anything but do something.
def meow():  # This is a function. Like what you learn in Maths
    print('meow!')  # Add space before a line when you're in a function

def two_stuff(x, y):  # This is a function with parameters
    print(x, y)  # You can use the parameters! print() is a function that prints everything you give it.

# These are the functions that give you back something
def pass_the_ball_back(ball):
    return ball # Yep, it just returns the ball.

def sum(x, y):
    return x + y # It returns the result of x + y

# This is a functions that DOES NOTHING!
def i_will_pass():
    pass # pass is a special word. it's like, "meh, I'll pass and do nothing".

# defining the function isn't enough. You need to call it.
meow()
two_stuff(1, 2) # You need to give the parameters - x and y.
## TODO: make two_stuff print 3, 4

# when function returns value, you can give it to a variable
the_ball = pass_the_ball_back('Football')
## TODO: pass 'basketball' to pass_the_ball_back and get it.

# or just give the result to another function
print(sum(1, 2))


# You can even do this
print(sum(sum(1, 2), sum(3, 4)))
