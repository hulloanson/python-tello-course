from telloswarm.models import Drone
# More complex structure

## Array
numbers = [1,2,3]

words = ['meow?', 'nya!', 'にゃー', '喵']

mixed  = [1, 2, '哎呀']

first_number = numbers[0]
second_number = numbers[1]
### TODO Third number? Write here.

### TODO Also get the 4th word

## Dict
ages = {
    'Carrie': 62,
    'Winnie': 66,
    'Edward': 28,
    'Agnes': 23,
}

carrie_age = ages['Carrie']
### TODO get the ages of Winnie, Edward, and Agnes

### TODO Your turn - get second drone's sn

self_intros = [
    {
        'name': 'Anson',
        'age': 3,
        'job': 'Baby',
    },
    ### TODO Add your self intro here!
]

### TODO: Print your own self_intro here