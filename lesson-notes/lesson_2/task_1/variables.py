# Variables

number = 1 # This is a variable. it has a name and a value.
# Obviously, the name is "number" and the value is 1

# It can also has a "string".
# Which means whatever you can type into it between a pair of quotes
number = 'a crazy cat jumps over a sleeping dog' # You can use a single quote
number = "a crazy cat jumps over a sleeping dog" # or a double quote

print("number is", number)

# It doesn't make sense to call a "string" a "number" right?
# Yes, you can use other names as well. 
# Just start it with any non-numerical character.
sentence = 'cat goes meow, dog goes woof'
# You can even use Chinese
sentence = '食咗飯未呀'
# Or Japanese
挨拶 = 'こんにちは'

# The variable name can't have space though.
# If your variable name has 2 words, do this:
ohyes = 'oh~yeah~'
# or this:
where_am_i = 'who knows!'

# K. Play time. Define some variables as you like
# Avoid names like a1, b, c. WHAT DO THEY EVEN MEAN?!
#### Type here, don't be shy


#### Done?
