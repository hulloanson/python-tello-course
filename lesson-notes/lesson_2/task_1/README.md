# Lesson 2 - Task 1
## Goal
Write your (first) Python program

## Get started!
It's quite a bit to digest. Look at the [Python cheatsheet](../../python-cheatsheet.md) and listen to your tutor carefully!

Ready? Let's dive.

## Mini-tasks
### Mini-task 1A - Variables, data structure
1. Go to Visual Studio Code
2. Ctrl-P -> variables<i></i>.py -> enter
3. Ctrl-Shift-P -> debug current file -> enter

### Mini-task 1B - functions
functions<i></i>.py

### Mini-task 1C - complex data structures
complex<i></i>.py


## Continue to Task 2!
[Swoosh](../task_2/README.md)
