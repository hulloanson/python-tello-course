# Lesson 2 - Task 4
## Goal
Move Tello with Python!

## Steps (in Python!)
1. Open `move.py` in vscode.
2. Write some code so it moves forward 100cm.
    - Caution! Leave enough room for the Tellos

## Assessment
Show it to your lecturer / assistant : )

## Any problems? Solutions?
Jot it down in your notebook / cheatsheets!

## Continue to Task 5!
[Here](../task_5/README.md)
