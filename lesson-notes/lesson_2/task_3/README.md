# Lesson 2 - Task 3
## Goal
Take off and land with Python

## Steps (in Python!)
This is how we run the take off and land instructions.

1. Open the python file: In Visual Studio code (we'll call it vscode from now on), press `Ctrl-P` -> type `takeoff_land.py` -> enter
2. Now we have the code.
3. We will write a few lines of code. To run it, press the green play button:
![Run takeoff_land.py](../../../images/run_takeoff_land_py.png)

## Of course, we need to know the code!
Read carefully the comments in `takeoff_land.py`. They explains what the code does. When in doubt, ask your tutor!

## And yes, it's not complete.
Complete all the TODOs so it works correctly : )

## Assessment
Show it to your lecturer / assistant : )

## Any problems? Solutions?
Jot it down in your notebook / cheatsheets!

## Continue to Task 4!
[Here](../task_4/README.md)
