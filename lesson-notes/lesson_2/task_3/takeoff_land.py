from telloswarm.scan import scan
from telloswarm.connection import start_connections, close_connections
from telloswarm.commands import *

### The must-have lines at the start in each drone program
# TODO: replace with your drone's serial number
serial_numbers = [
    '0TQDG38EDBDW1U'
]

# Scan for the drones with the serial numbers as defined above.
drones = scan(serial_numbers)

# Connect to the drones
connections = start_connections(drones)
### End of the must-have blocks at the start

# TODO: get connection of your own drone.
connection = connections['0TQDG38EDBDW1U']

# TODO: call the functions! Have a look at commands.py (ctrl-P -> commands.py -> ctrl-enter)

### The must have line at the end of each drone program: end the connections
close_connections(connections)