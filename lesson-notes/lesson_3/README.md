# Lesson 3
## Mission Pad!
The pad with bubbles, rocket, astronaut and a strangely shaped number.

## What are mission pads?
A pad with certain pattern on it. Tello Edu could recognize the up and down and the ID of a pad with by analyzing video stream from its belly camera.

## The X, Y, Z of a pad
<!-- TODO: copy the image from Tello mission pad doc -->

## Tasks
1. [Task 1](task_1/README.md)
2. [Task 2](task_2/README.md)
3. [Task 3](task_3/README.md)
4. [Task 4](task_4/README.md)

## Takeaways
See this lesson's [summary](summary.md)