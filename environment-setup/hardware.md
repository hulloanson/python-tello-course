# Hardware Environment setup
This describe the hardware environment used in this course and how it could be set up.

## Equipments
For each swarm during the lessons:

|Equipment|Amount|Remark|
|---|---:|---|
|Router|1|-|
|Tello Edu|min. 1|-|
|Batteries|min. 3 @ drone|-|
|Charging slots|2 @ drone|-|
|PC with Windows 10|1|-|

## Network Hardware
Tello communicates through WI-FI + IP + UDP. Transmission is therefore unreliable. It is prone to interferences. However, a moderately-priced WI-FI router from trustworthy network equipment brand would normally suffice at around 20 drones within ~300 sq.ft. environment without tall obstacles.

## Network topology
It is how each swarm arrange its network.

![Drone-Router-PC topology](../images/PC-router-drones.svg)

Each network can only have one PC. Multiple PCs in one network would compete for drones and it is hard to predict which will get which.

### Internet
If you want the PC to access the internet, plugging in the WAN port does not affect drone control. 

In a classroom settings, where there are multiple groups, i.e. multiple swarms, it might be desirable to arrange a router as internet gateway: LAN outputs of the gateway connects to WAN port of each router to provide internet access. Caution should be taken to NOT use the same subnet for internet and the router's local network.

## Drone setup
Rules generally follows those detailed in the [Tello cheatsheet](../cheatsheets/tello.md) and the [Cautions cheatsheet](../cheatsheets/cautions.md).