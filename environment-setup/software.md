# Software Environment setup
This describe the software environment used in this course and how it could be set up.

## Overview
This setup involves:
1. A CPU that supports Intel's VT-x / AMD's AMD-V virtualization technology. To put it bluntly, any PC after 2015 will do.
1. Windows 10, preferably with the latest update.
1. Ubuntu 18.04 running in VMWare's Workstation Player
2. Python 3.8.0 managed by [pyenv](https://github.com/pyenv/)

<!-- TODO: bare metal install -->
<!-- ### Before you start though...
You could also install a bare metal Ubuntu and install the necessary software.
 -->
## Disclaimer
The following steps might contains broken link, outdated steps due to changes in websites and softwares, etc. Users are advised to improvise should such unfortunate event occurs.

## Steps
### Enable Virtualization Technology in BIOS
Though usually enabled by default, it does us good to actually check just in case.

We typically enter the BIOS by pressing keys like F1, F2, ESC, F10, Delete **immediate after we press the power on button**. When in doubt, try pressing all of these keys in quick alternation; if failed, try key combinations like Ctrl+Alt+Delete or Ctrl+Alt+Esc on next boot.

Navigate every tab, every menu item. There has to be an entry that includes one of these terms: "virtualization technology", "VT-X" or "AMD-V". If there isn't, your CPU does not support virtualization technology (and has to be pretty old indeed). Make sure the entry is enabled / turned on.

### Update Windows 10 to the latest version
Author uses the 1903 update at the time of writing.

### Disable Hyper-V
Hyper-V and VMWare are not compatible.

Navigate to "Turn Windows feature on or off". Make sure Hyper-V is not ticked.

### Disable device/credential guard
It is said enabled Hyper-V could also causes this. Please make sure you have disabled Hyper-V before continuing.

Download the [Windows Defender Device Guard and Windows Defender Credential Guard hardware readiness tool](https://www.microsoft.com/en-us/download/details.aspx?id=53337)

Follow instructions [here](https://web.archive.org/web/20191226050737/https://docs.microsoft.com/en-us/windows/security/identity-protection/credential-guard/credential-guard-manage#turn-off-with-hardware-readiness-tool). 

### Install VMWare Workstation Player
Download the 64-bit Windows Installer on this page:

https://www.vmware.com/go/downloadworkstationplayer

Install as per the default instructions. You need administrator privileges.

### Download the VM
Download the virtual machine [here](https://drive.google.com/drive/folders/1ItFJEVGpvmHf_-fviQfqPtJKjp5O9IaH?usp=sharing). Make sure you have at least 8GB free disk space.

Unzip the downloaded zip to wherever you desire.

### Open the virtual machine
Launch VMWare Workstation Player, Click "Open Virtual Machine", find the unzipped folder and open the Ubuntu.vmx inside.

Do not start it yet.

### Adjust network settings
1. Click "Network Adapter 2". 
2. Make sure "Bridged" is selected and "Replicate physical network connection state" ticked.
3. Press "Configure Adapters". Select only the one that connects to the router (e.g. the WI-FI adapter, or the ethernet adapter)
4. Press OK to save settings.

### Launch it!
Press play. VMWare Player asks you some questions when you first run the VM. Look at [how you should reply](#first-time-launching).

### First-time launching
#### Moved or Copied?
Copied.
#### VMWare Tools for Linux. Install?
Download and Install. Press hide if you don't want to wait. You might be reminded from time to time to update in the future too.

## Next
[How to use the VM](../cheatsheets/vm-usage.md)

## Steps used to built the VM
*This section is not written because other documentations take precedence. Interested parties are welcome to contact Anson Yuen (hulloanson@gmail.com). He'll be happy to discuss the setup and other things.*
