# READ THIS
Tellos are fragile. So are you. Play safe. Remember by rote EVERY SINGLE THING written here!

## The list
### What to do if your Tello go crazy
Snatch the side of your tello from the under. **SNATCH THE SIDE, FROM THE UNDER**, always. No one is responsible if you insist otherwise and cut off your fingers.

### Turn off your Tello when you're not using it
Tello is really power hungry and the batteries aren't very large. Turn it off every time when you are not using your Tello so you don't waste batteries.

### Charge batteries whenever you can
You only have those few batteries. Keep them full so you don't need to wait for the looooong charge.

### Make room for Tello
Make sure each Tello has enough room - at least 1m around to fly!