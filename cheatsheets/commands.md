# Command cheatsheet
This cheatsheet concentrates on how you use the Python functions to command Tello drones. There are 2 ways - the Python way and the UDP way. Distinctions are discussed at the [end of this cheatsheet](#tello-commands)

In this course, there are 2 ways of telling a Tello what to do:

1. Sending command through UDP (a.k.a the Packet Sender way)
2. Calling Python functions by importing functions from `telloswarm.commands`.

Both are highly similar - sdk commands are directly mapped to function of the same name - `takeoff`, `forward`, etc... For example:

The UDP way:
```plaintext
forward 100
```

The Python way:
```python
forward(connection, 100)
```

## Commands
### Starting and ending lines
You will always need to use this template to execute a Tello drone flight in this course.
```python
from telloswarm.scan import scan
from telloswarm.connection import start_connections, close_connections
from telloswarm.commands import *

# Replace with your drone's serial number
serial_numbers = [
    '0TQDG38EDBDW1U'
]

# Scan for the drones with the serial numbers as defined above.
drones = scan(serial_numbers)

# Connect to the drones
connections = start_connections(drones)

#### Tello commands start #### 

#### Tello commands end #### 

### End the connections
close_connections(connections)
```

### Basic
Here are the functions as to be imported from `telloswarm.commands`. Most are self-explanatory. Comments will be added where it need explaining.

#### Basic
```python
# c means connection
takeoff(c)
land(c)
# up, down, left, right, forward, back have range of 20 - 500 cm
up(c, 100)
down(c, 100)
left(c, 100)
right(c, 100)
forward(c, 100)
back(c, 100)
# cw and ccw have range of 1-360 degree
cw(c, 180)
ccw(c, 180)
```

<!-- TODO: complete and remove warning -->
_**Writer's note: not complete yet from here**_

#### Using coordinates
```python
# go: go to coordinates x, y and z. Origin is the current position. 
# Speed has to be specified too.
go(c, 100, 100, 100, 40) # x: 100, y: 100, z: 100, speed: 40
# Fly a curve give 3 points:
# The origin: drone's position. 0,0,0
# Point 2 and point 3: arbitrary points
# Origin, point 2 and point 3 should form an arc of radius between 0.5m and 100m
curve(c, 
    0, 50, 50, # x1, y1, z1
    0, 100, 0, # x2, y2, z2
    40,
    wait_for=15, # You might have to wait longer for the curve to complete
)
```

##### Illustration: Curve
Any circle, or say arc, can be represented by 3 points.

Tello fly from the Start point - which is **always the current position**,  through the Intermediate point, to the End point.

Some curves to illustrate the idea:

![2d curve](./../images/2d-curve.svg)

How about 3d curves? Here's an 180 deg curve, tilted 45 deg:

<div style="display: flex; align-items: center; width: 100%">

<img src="../images/tilted-curve-orthogonal.svg" 
    style="flex-basis: 250px; flex-grow: 1"
/>

<span style="padding: 0 20px">
</span>

<img src="../images/tilted-curve-sidelook.svg" 
    style="flex-basis: 250px; flex-grow: 1"
/>

</div>

How about a complete circle? You need to calculate a new 180 deg curve, with the old End as the new Start (0, 0, 0). The coordinates would be: 

```plaintext
Start: (0, 0, 0) 
Middle: (r * cos(45 deg), -r, - r * sin(45 deg))
End: (0, -2r, 0)
```

Plot a simple 3d graph if you don't understand!

#### Using pad
```python
# The UDP command is still "curve", but with addition mission pad id argument.
# Here we make the distinction for clarity
curve_pad(c,
    0, 50, 50, # x1, y1, z1
    0, 100, 0, # x2, y2, z2
    40, 'm1', # speed: 40, mission_pad: m1 (note the string!)
)
# The UDP command is still "go", but with addition mission pad id argument.
# Here we make the distinction for clarity
go_pad(c,
    100, 100, 100, 40, # x: 100, y: 100, z: 100, speed: 40
    'm1', # mission_pad: m1
)
# Fly to x, y, z of mission pad 1, then 0, 0, z of mission pad 2
jump(c,
    100, 150, 140, # x: 100, y: 150, z: 140
    40, # speed
    90, # yaw = rotate to 90 deg of the mission pad 2
    'm3', 'm4' # use mission pad 3 as the first pad, mission pad 4 as the second pad
)
```

#### Stopping
```python
# Land immediately no matter what
emergency(c)
# Stop whatever the drone is doing and hovers in air
stop(c)
```
