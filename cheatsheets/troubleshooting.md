# Troubleshooting Cheatsheet

## My Tello's light didn't turn green / it didn't restart after the ap command / it didn't respond

Your Tello didn't receive the packet

1. Your Tello wasn't ready yet
    - Solution: try again when it's ready
2. Your Tello had slept
    - How do you see it: No lights.
        - Fun fact: blinking red light means you've ignored the Tello for too long
    - Solution: turn it on again : )
3. Your Tello had run out of battery
    - Usually it falls asleep fast if battery is on low
    - Solution: switch batteries!
4. You connected to the wrong Tello