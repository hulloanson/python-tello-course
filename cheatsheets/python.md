# Python Cheatsheet
## Python basic blocks: variables, functions
### Variables
A variable is a name that represents a value.

See [Python Cheatsheet - Variables](../../cheatsheets/python.md#variables) to review.

⚠️ When naming variables:
1. Use meaningful names
2. Separate words with underscore (`_`) for readability
3. Do not start with arabic numbers
```python
# Assigning values
number = 1 # number is the name, 1 the value
moe_animal = 'neko'
# Using variables
cute_animal = moe_animal # cute_animal is now also 'neko'
```

### Functions
A function remembers whatever you tells it to do and does it when you call it.

⚠️ Same rule of thumb as variables applies when naming.
```python
# Defining
def meow_and_return_sum(x, y): # x and y are parameters. You can have none or a lot
    print('meow')
    print('miaow')
    return x + y

# Using
sum = meow_and_return_sum(1, 2) # return value assigned to variable sum
print(meow_and_return_sum(1, 2)) # prints the return value
meow_and_return_sum(1, meow_and_return_sum(2)) # return value used as parameter
```

## Data types
A value can be of different data types.
### Numbers
```python
integer = 1
decimal = 1.1
# arithmetic operations
multiplied_number = 1 * 2
added_number = 1 + 2
divided number = 1 / 2
subtracted_number = 1 - 2
```
### String
Words, sentences. ALWAYS quoted. single quote, or double quote.
```python
cat_name = "Fatty"
the_almighty = 'God'
question = "Aren't they the same?"
```
### List (also called Array)
A list of values. Hold between a pair of square brackets.
```python
numbers = [1,2,3]
# A mixed bag too
mixed = [1, "word", "oooolala"]
# Get numbers
first_number = numbers[0]
second_number = numbers[1]
```
### Dictionary
A set of key-value pairs.
```python
shapes = {
    'ball': 'sphere',
    'rubik': 'square',
}
# Get values
ball_shape = shapes['ball']
```

## Important concepts
### Indent
First of all:

Indents in Python is **IMPORTANT**. Mess them up, the program doesn't run.

### What are indents?
Spaces before a line. For example:
```
Not indented 
    Indented (first level of indent)
        More indented (second level of indent)
            Even more indented (third level indent)
```
In Python:

Something indented means it's inside the less indented thing aboves it. For example:
```python
def meow():
    print('meow')

print('woof')
```
We can say:

`print('meow')` is inside the function `meow()`. `print('woof')` is not.

Every level of indent needs to have same amount of space. But never mind. In real life you press `Tab` (more indent) or `Shift-Tab` (less indent) and it's done for you correctly and automatically.

### Scope
Variables only exists within their own scope.

Scopes are represented by indents. For example:

```python
# "Global" scope starts.
dog_sound = 'woof'

def meow():
    # function meow's scope starts
    cat_sound = 'meow'
    print(cat_sound) # cat_sound exists here because it's in function meow's scope
    print(dog_sound) # dog_sound exists here, since the global scope covers everything
    # function meow's scope ends

# cat_sound does not exists here because it's out of meow's scope

def talk():
    # function talk's scope starts
    human_sound = 'blablabla'
    print(human_sound)
    print(dog_sound)
    # function talk's scope ends

# Does not print 'blablabla'. human_sound only exists in function talk's scope
print(human_sound)

# Global scope ends.
```

### Imports
You can imports variables and functions from other modules.

For example, the module we use for drone programming in this course:

⚠️ Imports typically stays on top of a Python program.

```python
import time # Import module time
time.sleep(1) # Sleep for 1 second

import telloswarm.comm # Import a module telloswarm's submodule comm
# Use scan function from telloswarm module
comm.do()

# You can also import just the ones you specify
from telloswarm.scan import scan # Import scan from telloswarm's submodule scan

scan(['a certain serial number']) # Note we don't have to write module name + dot before it
```