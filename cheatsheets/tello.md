# Tello cheatsheet
Good students cheat. Serve yourself : )

## What is the AP name?
Remove the battery. It's written in the battery holder

## What is the serial number (sn)?
Remove the battery. It's written in the battery holder, above the Wifi name

## What is my Tello's IP and port
- When it is in AP mode: `192.168.10.1`

- When it is in station mode: [scan for it](#scan-for-your-tellos)

- The port is always `8889`

## Scan for your Tellos
1. Visual Studio Code
2. Ctrl-Shift-P -> python create terminal
2. Run the scan utility:
```bash
python utils/scan_tello.py [one or more serial numbers]
```
e.g. ```python utils/scan_tello.py 0TQDG7BEDBJ740```

4. Drone with their IPs should appear under `Results`, e.g.
```text
Results:
-----------------
Drone 0TQDG7BEDBJ740 at 192.168.0.110
That's it. No more.
```
3. Turn off and turn on your Tello

## Send a command to Tello in Packet Sender
1. Type command you want to send in the ASCII box.
1. Type the IP address and port of your Tello. ([How to find your Tello's IP](#what-is-my-tellos-ip-and-port))
2. Confirm it's UDP
3. Send

## Save the packets
1. Type command, IP address and port
2. Give it a name
3. Press save.
4. Now you see the packet saved. Next time, sending it is just the matter of clicking "Send"!

## Reset your Tello (it returns to AP mode)
1. Turn it on
2. Long press power button until light turns off
3. Turn it on again

## Differences between AP mode and station mode
1. AP mode: Tello shoots its own Wifi
2. Station mode: Tello connects to a Wifi

## Turn on command mode
1. Turn on your Tello. It is now in AP mode (what? don't know what is AP mode? [Read the Tello cheatsheet](../../../cheatsheets/tello.md#differences-between-ap-mode-and-station-mode)).
2. Connect to your Tello's AP ([Tello Cheatsheet - What is the AP name?](#what-is-the-ap-name))
3. Start Packet Sender
4. Send `command` to your Tello ([Refer to Tello cheatsheet - Send a command](../../../cheatsheets/tello.md#send-a-command-to-tello-in-packet-sender))

### What should happen?
Your Tello should now blink a steady green light


## Switch to station mode
1. Launch Packet Sender
2. Turn on Tello's command mode
3. Send `ap [wifi name] [wifi password]` to the Tello
    
    e.g. `ap wifihere wifipassword`

### What should happen?
Your Tello should 
1. Returns a OK to Packet Sender
2. Restarts and connects to your router.

