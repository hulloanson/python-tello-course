# VM Usage
This is a really simplistic guide, telling you the absolute minimum to go through the Tello course.

## Login
There is only one user: `luvdrones`. The password is `abc`.

## Pinned Apps
They appear at the left side of the screen.

1. Firefox - web browsing, note reading
2. Visual Studio Code - Coding
3. File explorer - self explanatory
4. Packet Sender - Sending packets to IP
5. Terminal - Hacker-ish cool-ish black screen command typing

## Code
Your can find it on Desktop > telloswarm

<!-- For a walkthrough of how you code, [click here]() -->

## Input methods
Press `Win+space` to switch between input methods. There are 4 of them:

1. The classic English keyboard (QWERTY)
2. Cangjie (倉頡)
3. Quick (速成)
4. The less commonly used English keyboard (DVORAK)

That's it. The remainder of it play with it yourself :)
