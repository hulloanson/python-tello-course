# Concepts
## How data are sent between your PC and Tello
1. AP mode:
   
   Drone <-> your PC

2. Station mode:

    Drone <-> Router <-> Your PC

## What is IP network?
Interconnected peers addressed by IP.

![Simple IP Network](../images/simple-ip-network.svg)

A network to send things to others.

> <u>Network</u> - When things are connected together

> <u>IP</u> - **I**nternet **P**rotocol

#### Protocol
> <u>Protocol</u> - A standard way to communicate

### How to find one another?
By **IP Address**

Example of IP address:

`192.168.0.110`

### How to actually send something to someone?
Send the thing to **IP Address** + **Port number**

> <u>Port number</u> - ranges from 1 - 65535.

We write it like this:

`192.168.0.110:8889`

Same for receiving. You wait at [ip address]:[port].

e.g. `192.168.0.105:9999`

### Analogy

<u>IP address</u> is like a address to a building;

<u>Port number</u> is like a flat in a building.

<u>IP Network</u> is like a city. Buildings connected by roads and highways. (WIFI and wires)

## UDP
IP Network just send whatever is put onto it.

Let's say I send this 2 messages:

1. 兒子生性

2. 病母安慰

On IP, the receiver would get: 兒子生性病母安慰

WHAT DOES THIS MEAN?!

### How to avoid this problem?
Put punctuation marks!

UDP is like putting quotes around the contents, so everyone knows messages are separate.

「兒子生性」

「病母安慰」

## So Tello?
Tello uses **UDP** to communicate on a **IP network**.

### When we send commands to Tello

`UDP: "command"` <- We call this a **packet** <small>(Of course not as simple as this but you get the picture)</small>

`IP: 192.168.10.1:8889`

So we send the **packet** to the **IP Address (with port number)**

### Then Tello receives the packet.

The packet has OUR information, e.g.

Source IP: 192.168.10.2:12345

(Here source = us obviously)

If the packet says `command`, Tello will ONLY responds to the source IP after that. Nowhere else. Until it restarts.

### Tello SDK commands and responses
Not written yet
<!-- TODO: sdk command & responses cheatsheet! -->